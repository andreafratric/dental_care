﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dental_Care.Data.Entities
{
    /// <summary>
    /// PatientEntity
    /// </summary>
    /// <seealso cref="Dental_Care.Data.Entities.EntityBase" />
    [Table("Patient")]
    public class PatientEntity : EntityBase
    {
        /// <summary>
        /// Gets or sets the exams.
        /// </summary>
        /// <value>
        /// The exams.
        /// </value>
        [InverseProperty("Patient")]
        public ICollection<ExamEntity> Exams { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientEntity"/> class.
        /// </summary>
        public PatientEntity()
        {
            Exams = new HashSet<ExamEntity>();
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [Required]
        [StringLength(30)]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        [Required]
        [StringLength(30)]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the date of birth.
        /// </summary>
        /// <value>
        /// The date of birth.
        /// </value>
        [Required]
        [Column(TypeName = "date")]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        [Required]
        [StringLength(maximumLength: 1)]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        /// <value>
        /// The phone number.
        /// </value>
        [Required]
        [StringLength(11, MinimumLength = 10)]
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        [Required]
        [StringLength(30)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the date of account creation.
        /// </summary>
        /// <value>
        /// The date of account creation.
        /// </value>
        [Required]
        [Column(TypeName = "date")]
        public DateTime DateOfCreation { get; set; }

        /// <summary>
        /// Gets or sets the number of cardboard.
        /// </summary>
        /// <value>
        /// The number of cardboard.
        /// </value>
        [Required]
        [Display(Name = "Broj kartona")]
        public string NumberOfCardboard { get; set; }
    }
}