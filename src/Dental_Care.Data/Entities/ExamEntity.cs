﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dental_Care.Data.Entities
{
    /// <summary>
    /// ExamEntity
    /// </summary>
    /// <seealso cref="Dental_Care.Data.Entities.EntityBase" />
    [Table("Exam")]
    public class ExamEntity : EntityBase
    {
        /// <summary>
        /// Gets or sets the date of exam.
        /// </summary>
        /// <value>
        /// The date of exam.
        /// </value>
        [Required]
        public DateTime DateOfExam { get; set; }

        /// <summary>
        /// Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [Required]
        public int PatientId { get; set; }

        /// <summary>
        /// Gets or sets the patient.
        /// </summary>
        /// <value>
        /// The patient.
        /// </value>
        [ForeignKey("PatientId")]
        [InverseProperty("Exams")]
        public PatientEntity Patient { get; set; }
    }
}