﻿using System.ComponentModel.DataAnnotations;

namespace Dental_Care.Data.Entities
{
    public class EntityBase
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [Key]
        public int Id { get; set; }
    }
}