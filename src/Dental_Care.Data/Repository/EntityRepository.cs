﻿using Dental_Care.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dental_Care.Data.Repository
{
    /// <summary>
    /// EntityRepository<T>
    /// </summary>
    /// <seealso cref="Dental_Care.Data.Repository.IRepository{Dental_Care.Data.Entities.PatientEntity}" />
    public class EntityRepository<T> : IRepository<T> where T : EntityBase
    {
        /// <summary>
        /// The context
        /// </summary>
        private readonly DentalCareContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public EntityRepository(DentalCareContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets all T.
        /// </summary>
        /// <returns></returns>

        public List<T> GetAll()
        {
            List<T> entities = context.Set<T>().ToList();
            return entities;
        }

        /// <summary>
        /// Gets T by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>

        public T GetById(int id)
        {
            T entity = context.Set<T>().Find(id);
            return entity;
        }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Create(T entity)
        {
            if (entity != null)
            {
                try
                {
                    context.Set<T>().Add(entity);
                    context.SaveChanges();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Edits the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="entity">The entity.</param>

        public void Edit(int id, T entity)
        {
            if (entity != null)
            {
                try
                {
                    context.Set<T>().Update(entity);
                    context.SaveChanges();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>

        public void Delete(T entity)
        {
            if (entity != null)
            {
                try
                {
                    context.Set<T>().Remove(entity);
                    context.SaveChanges();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
    }
}