﻿using Dental_Care.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Dental_Care.Data.Repository
{
    public class PatientRepository : EntityRepository<PatientEntity>
    {
        private readonly DentalCareContext context;

        public PatientRepository(DentalCareContext context) : base(context)
        {
            this.context = context;
        }

        public string GetPatientName(int id)
        {
            IEnumerable<PatientEntity> patients = context.PatientEntities;
            if (patients==null)
            {
                return null;
            }
            return patients.Where(p => p.Id == id).Select(p1=>$"{p1.FirstName} {p1.LastName}").SingleOrDefault();            
        }
    }
}