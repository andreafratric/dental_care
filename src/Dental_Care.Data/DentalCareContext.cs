﻿using Dental_Care.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Dental_Care.Data
{
    /// <summary>
    /// DentalCareContext
    /// </summary>
    /// <seealso cref="Microsoft.EntityFrameworkCore.DbContext" />
    public class DentalCareContext : DbContext
    {
        /// <summary>
        /// Gets or sets the patient entities.
        /// </summary>
        /// <value>
        /// The patient entities.
        /// </value>
        public virtual DbSet<PatientEntity> PatientEntities { get; set; }

        /// <summary>
        /// Gets or sets the exam entities.
        /// </summary>
        /// <value>
        /// The exam entities.
        /// </value>
        public virtual DbSet<ExamEntity> ExamEntities { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DentalCareContext"/> class.
        /// </summary>
        /// <param name="options">The options.</param>
        public DentalCareContext(DbContextOptions<DentalCareContext> options) : base(options)
        { }

        /// <summary>
        /// Override this method to further configure the model that was discovered by convention from the entity types
        /// exposed in <see cref="T:Microsoft.EntityFrameworkCore.DbSet`1" /> properties on your derived context. The resulting model may be cached
        /// and re-used for subsequent instances of your derived context.
        /// </summary>
        /// <param name="modelBuilder">The builder being used to construct the model for this context. Databases (and other extensions) typically
        /// define extension methods on this object that allow you to configure aspects of the model that are specific
        /// to a given database.</param>
        /// <remarks>
        /// If a model is explicitly set on the options for this context (via <see cref="M:Microsoft.EntityFrameworkCore.DbContextOptionsBuilder.UseModel(Microsoft.EntityFrameworkCore.Metadata.IModel)" />)
        /// then this method will not be run.
        /// </remarks>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PatientEntity>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.DateOfCreation).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<ExamEntity>(entity =>
            {
                entity.HasOne(d => d.Patient)
                    .WithMany(p => p.Exams)
                    .HasForeignKey(d => d.PatientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Exam__PatientId__160F4887");
            });
        }
    }
}