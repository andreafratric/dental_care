﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dental_Care.Models
{
    public class Patient
    {
        /// <summary>
        /// Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        public int PatientId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [Required(ErrorMessage = "Unesite ime pacijenta")]
        [Display(Name = "Ime")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        [Required(ErrorMessage = "Unesite prezime pacijenta")]
        [Display(Name = "Prezime")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the date of birth.
        /// </summary>
        /// <value>
        /// The date of birth.
        /// </value>
        [Required(ErrorMessage = "Unesite datum rođenja pacijeta")]
        [Display(Name = "Datum rođenja")]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        [Required(ErrorMessage = "Odaberite pol pacijeta")]
        [Display(Name = "Pol")]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        /// <value>
        /// The phone number.
        /// </value>
        [Required(ErrorMessage = "Unesite broj telefona pacijeta")]
        [StringLength(11, MinimumLength = 10, ErrorMessage ="Min broj karaktera je 10, max 11 ")]
        [Display(Name = "Broj telefona")]
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        [Required(ErrorMessage = "Unesite email pacijeta")]
        [EmailAddressAttribute(ErrorMessage = "Unesite pravilan format email-a")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the date of account creation.
        /// </summary>
        /// <value>
        /// The date of account creation.
        /// </value>
        public DateTime DateOfCreation { get; set; }

        /// <summary>
        /// Gets or sets the number of cardboard.
        /// </summary>
        /// <value>
        /// The number of cardboard.
        /// </value>
        [Required(ErrorMessage = "Unesite broj kartona")]
        [Display(Name = "Broj kartona")]
        public string NumberOfCardboard { get; set; }
    }
}