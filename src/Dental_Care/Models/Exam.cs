﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dental_Care.Models
{
    /// <summary>
    /// Exam
    /// </summary>
    public class Exam
    {
        /// <summary>
        /// Gets or sets the exam identifier.
        /// </summary>
        /// <value>
        /// The exam identifier.
        /// </value>
        public int ExamId { get; set; }

        /// <summary>
        /// Gets or sets the date of exam.
        /// </summary>
        /// <value>
        /// The date of exam.
        /// </value>
        [Required(ErrorMessage = "Unesite datum pregleda")]
        [Display(Name = "Datum pregleda")]
        public DateTime DateOfExam { get; set; }

        /// <summary>
        /// Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        /// The patient identifier.
        /// </value>
        [Required(ErrorMessage = "Unesite id pacijenta")]
        [Display(Name = "Id pacijenta")]
        public int PatientId { get; set; }

        public string Name { get; set; }
    }
}