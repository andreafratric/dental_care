﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dental_Care.Models
{
    public class Service
    {
        /// <summary>
        /// Gets or sets the service identifier.
        /// </summary>
        /// <value>
        /// The service identifier.
        /// </value>
        public int ServiceId { get; set; }

        /// <summary>
        /// Gets or sets the name of service.
        /// </summary>
        /// <value>
        /// The name of service.
        /// </value>
        public string NameOfService { get; set; }

        /// <summary>
        /// Gets or sets the price of service.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets the exam identifier.
        /// </summary>
        /// <value>
        /// The exam identifier.
        /// </value>
        public int ExamId { get; }
    }
}
