﻿using Dental_Care.Data.Entities;
using Dental_Care.Data.Repository;
using Dental_Care.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

/// <summary>
/// ExamsController
/// </summary>
namespace Dental_Care.Controllers
{
    public class ExamsController : Controller
    {
        /// <summary>
        /// The repository
        /// </summary>
        private readonly IRepository<ExamEntity> repository;

        /// <summary>
        /// The repository patients
        /// </summary>
        private readonly PatientRepository repositoryPatients;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExamsController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public ExamsController(IRepository<ExamEntity> repository,
            PatientRepository repositoryPatients)
        {
            this.repository = repository;
            this.repositoryPatients = repositoryPatients;
        }

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            //IEnumerable<PatientEntity> patientsEntity = repositoryPatients.GetAll();
            List<ExamEntity> examEntities = repository.GetAll();
            if (examEntities == null)
            {
                return View();
            }
            List<Exam> examsModel = new List<Exam>();
            foreach (ExamEntity eEntity in examEntities)
            {
                Exam eModel = new Exam()
                {
                    ExamId = eEntity.Id,
                    DateOfExam = eEntity.DateOfExam,
                    PatientId = eEntity.PatientId,
                    Name = repositoryPatients.GetPatientName(eEntity.PatientId)
                };
                examsModel.Add(eModel);
            }

            return View(examsModel);
        }

        /// <summary>
        /// Detailses the specified exam.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IActionResult Details(int id)
        {
            ExamEntity eEntity = repository.GetById(id);
            if (eEntity == null)
            {
                return NotFound();
            }
            Exam eModel = new Exam()
            {
                ExamId = eEntity.Id,
                DateOfExam = eEntity.DateOfExam,
                PatientId = eEntity.PatientId,
                Name = repositoryPatients.GetPatientName(eEntity.PatientId)
            };
            return View(eModel);
        }

        /// <summary>
        /// Creates exam instance.
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            IEnumerable<PatientEntity> patientsEntity = repositoryPatients.GetAll();
            ViewBag.Poruka = null;
            if (patientsEntity == null)
            {
                ViewBag.Poruka = "Prvo morate dodati pacijenta pa onda mu dodeliti pregled";
                return View();
            }
            List<SelectListItem> names = new List<SelectListItem>();
            foreach (var p in patientsEntity)
            {
                string id = p.Id.ToString();
                SelectListItem sl = new SelectListItem()
                {
                    Value = id,
                    Text = $"{p.FirstName} {p.LastName}"
                };

                names.Add(sl);
            }
            ViewBag.Names = names;
            return View();
        }

        /// <summary>
        /// Creates the specified exam.
        /// </summary>
        /// <param name="eModel">The e model.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Exam eModel)
        {
            int id = eModel.PatientId;
            PatientEntity pEntitySpecific = repositoryPatients.GetById(id);

            ViewBag.Poruka = null;

            if (pEntitySpecific == null)
            {
                ViewBag.Poruka = "Pacijent ne postoji pod izabranim id";
                return View();
            }
            if (ModelState.IsValid)
            {
                ExamEntity eEntity = new ExamEntity()
                {
                    Id = eModel.ExamId,
                    DateOfExam = eModel.DateOfExam,
                    PatientId = eModel.PatientId
                };
                repository.Create(eEntity);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        /// <summary>
        /// Edits the specified exam.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            ExamEntity eEntitySpecific = repository.GetById(id);

            if (eEntitySpecific == null)
            {
                return NotFound();
            }
            IEnumerable<PatientEntity> patientsEntity = repositoryPatients.GetAll();
            //ViewBag.Poruka = null;
            //if (patientsEntity == null)
            //{
            //    ViewBag.Poruka = "Nije pronađen pacijent";
            //    return View();
            //}
            List<SelectListItem> names = new List<SelectListItem>();
            foreach (var p in patientsEntity)
            {
                string idP = p.Id.ToString();
                SelectListItem sl = new SelectListItem()
                {
                    Value = idP,
                    Text = $"{p.FirstName} {p.LastName}"
                };

                names.Add(sl);
            }
            ViewBag.Names = names;

            Exam eModel = new Exam()
            {
                ExamId = eEntitySpecific.Id,
                DateOfExam = eEntitySpecific.DateOfExam,
                PatientId = eEntitySpecific.PatientId
            };
            return View(eModel);
        }

        /// <summary>
        /// Edits the specified exam.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="eModel">The e model.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Exam eModel)
        {
            ExamEntity eEntitySpecific = repository.GetById(id);
            try
            {
                eEntitySpecific.PatientId = eModel.PatientId;
                eEntitySpecific.DateOfExam = eModel.DateOfExam;
                repository.Edit(id, eEntitySpecific);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        /// Deletes the specified exam.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            ExamEntity eEntity = repository.GetById(id);
            if (eEntity == null)
            {
                return NotFound();
            }
            Exam eModel = new Exam()
            {
                ExamId = eEntity.Id,
                DateOfExam = eEntity.DateOfExam,
                PatientId = eEntity.PatientId,
                Name = repositoryPatients.GetPatientName(eEntity.PatientId)
            };
            return View(eModel);
        }

        /// <summary>
        /// Deletes the specified exam.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, Exam eModel)
        {
            try
            {
                ExamEntity eEntity = repository.GetById(id);
                repository.Delete(eEntity);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}