﻿using Dental_Care.Data.Entities;
using Dental_Care.Data.Repository;
using Dental_Care.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

/// <summary>
/// PatientsController
/// </summary>
namespace Dental_Care.Controllers
{
    public class PatientsController : Controller
    {
        /// <summary>
        /// The repository<PatientEntity>
        /// </summary>
        private readonly IRepository<PatientEntity> repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientsController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public PatientsController(IRepository<PatientEntity> repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Returns the list of existing patients.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            string g = "";
            List<PatientEntity> patientsEntity = repository.GetAll();
            List<Patient> patientsModel = new List<Patient>();
            foreach (PatientEntity pEntity in patientsEntity)
            {
                if (pEntity.Gender == "M")
                {
                    g = "Muški";
                }
                else if (pEntity.Gender == "F")
                {
                    g = "Ženski";
                }
                Patient pModel = new Patient()
                {
                    PatientId = pEntity.Id,
                    FirstName = pEntity.FirstName,
                    LastName = pEntity.LastName,
                    DateOfBirth = pEntity.DateOfBirth,
                    Gender = g,
                    Phone = pEntity.Phone,
                    Email = pEntity.Email,
                    NumberOfCardboard = pEntity.NumberOfCardboard,
                    DateOfCreation = pEntity.DateOfCreation
                };
                patientsModel.Add(pModel);
            }

            return View(patientsModel);
        }

        /// <summary>
        /// Detailses the specified patient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            PatientEntity pEntitySpecific = repository.GetById(id);
            if (pEntitySpecific.Gender == "M")
            {
                ViewBag.Gender = "Muški";
            }
            else if (pEntitySpecific.Gender == "F")
            {
                ViewBag.Gender = "Ženski";
            }
            return View(pEntitySpecific);
        }

        /// <summary>
        /// Creates a new instance of the patient.
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Creates a new instance of the patient.
        /// </summary>
        /// <param name="pModel">The p model.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Patient pModel)
        {
            if (ModelState.IsValid)
            {
                PatientEntity pEntity = new PatientEntity()
                {
                    Id = pModel.PatientId,
                    FirstName = pModel.FirstName,
                    LastName = pModel.LastName,
                    DateOfBirth = pModel.DateOfBirth,
                    Gender = pModel.Gender,
                    Phone = pModel.Phone,
                    Email = pModel.Email,
                    NumberOfCardboard = pModel.NumberOfCardboard,
                    DateOfCreation = pModel.DateOfCreation
                };

                repository.Create(pEntity);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        /// <summary>
        /// Edits the specified patient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            PatientEntity pEntitySpecific = repository.GetById(id);

            if (pEntitySpecific == null)
            {
                return NotFound();
            }

            Patient pModel = new Patient()
            {
                PatientId = pEntitySpecific.Id,
                FirstName = pEntitySpecific.FirstName,
                LastName = pEntitySpecific.LastName,
                DateOfBirth = pEntitySpecific.DateOfBirth,
                Gender = pEntitySpecific.Gender,
                Phone = pEntitySpecific.Phone,
                Email = pEntitySpecific.Email,
                NumberOfCardboard = pEntitySpecific.NumberOfCardboard,
                DateOfCreation = pEntitySpecific.DateOfCreation
            };

            return View(pModel);
        }

        /// <summary>
        /// Edits the specified patient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pModel">The p model.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Patient pModel)
        {
            PatientEntity pEntitySpecific = repository.GetById(id);
            try
            {
                pEntitySpecific.FirstName = pModel.FirstName;
                pEntitySpecific.LastName = pModel.LastName;
                pEntitySpecific.DateOfBirth = pModel.DateOfBirth;
                pEntitySpecific.Gender = pModel.Gender;
                pEntitySpecific.Phone = pModel.Phone;
                pEntitySpecific.Email = pModel.Email;
                pEntitySpecific.NumberOfCardboard = pModel.NumberOfCardboard;

                repository.Edit(id, pEntitySpecific);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        /// Deletes the specified patient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            PatientEntity pEntitySpecific = repository.GetById(id);
            if (pEntitySpecific.Gender == "M")
            {
                ViewBag.Gender = "Muški";
            }
            else if (pEntitySpecific.Gender == "F")
            {
                ViewBag.Gender = "Ženski";
            }
            return View(pEntitySpecific);
        }

        /// <summary>
        /// Deletes the specified patient.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, PatientEntity pEntity)
        {
            try
            {
                PatientEntity pEntitySpecific = repository.GetById(id);
                repository.Delete(pEntitySpecific);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}